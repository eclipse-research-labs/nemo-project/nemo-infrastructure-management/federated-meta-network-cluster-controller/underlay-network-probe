#	Copyright 2025
#	Universidad Politécnica de Madrid
#
#	Licensed under the Apache License, Version 2.0 (the "License");
#	you may not use this file except in compliance with the License.
#	You may obtain a copy of the License at
#
#	    http://www.apache.org/licenses/LICENSE-2.0
#
#	Unless required by applicable law or agreed to in writing, software
#	distributed under the License is distributed on an "AS IS" BASIS,
#	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#	See the License for the specific language governing permissions and
#	limitations under the License.

apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: network-probe-daemonset
  namespace: nemo-net
  labels:
    app: network-probe
spec:
  selector:
    matchLabels:
      app: network-probe
  template:
    metadata:
      labels:
        app: network-probe
    spec:
      serviceAccountName: network-probe-node-lister
      containers:
      - name: network-probe
        image: kaiser1414/network-performance-probe:latest
        ports:
            - containerPort: 5000
            - containerPort: 5001
        command: ['python', 'network_probe.py']
        args: ['--verbose', '--live', '--delay', '120', '--throughput', '--latency', '--packet-loss', '--failure', '--flask', '5000', '--prometheus', '5001']
        env:
        - name: PROBE_POD_NAME
          valueFrom:
            fieldRef:
              fieldPath: metadata.name
        - name: PROBE_POD_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
      restartPolicy: Always

---

apiVersion: v1
kind: ServiceAccount
metadata:
  name: network-probe-node-lister
  namespace: nemo-net

---

apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: network-probe-role
rules:
- apiGroups: [""]
  resources: ["nodes", "pods"]
  verbs: ["list", "get"]

---

apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: network-probe-rolebinding
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: network-probe-role
subjects:
- kind: ServiceAccount
  name: network-probe-node-lister
  namespace: nemo-net

---

apiVersion: v1
kind: Service
metadata:
  name: network-probe-metrics
  namespace: nemo-net
  labels:
    app: network-probe
spec:
  selector:
    app: network-probe
  ports:
  - name: metrics
    port: 5001
    targetPort: 5001

---

apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
  name: network-probe-metrics
  namespace: nemo-net
  labels:
    app: network-probe
spec:
  selector:
    matchLabels:
      app: network-probe
  endpoints:
  - port: metrics
    interval: 30s