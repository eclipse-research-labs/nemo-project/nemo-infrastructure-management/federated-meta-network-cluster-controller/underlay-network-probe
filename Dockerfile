#	Copyright 2025
#	Universidad Politécnica de Madrid
#
#	Licensed under the Apache License, Version 2.0 (the "License");
#	you may not use this file except in compliance with the License.
#	You may obtain a copy of the License at
#
#	    http://www.apache.org/licenses/LICENSE-2.0
#
#	Unless required by applicable law or agreed to in writing, software
#	distributed under the License is distributed on an "AS IS" BASIS,
#	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#	See the License for the specific language governing permissions and
#	limitations under the License.

# Use an official Python runtime as the base image
FROM python:3.9

# Set the working directory in the container
WORKDIR /app

# Copy the Python code into the container
COPY . /app

# Install any Python dependencies
RUN pip install -r requirements.txt

EXPOSE 5000 5001

CMD ["python", "network_probe.py", "--verbose", "--live", "--delay", "120", "--host", "127.0.0.1", "--throughput", "--latency", "--packet-loss", "--flask", "5000", "--prometheus", "5001"]